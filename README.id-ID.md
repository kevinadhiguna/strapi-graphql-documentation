Language : [🇺🇸](./README.md) | [🇯🇵](./README.ja-JP.md) | 🇮🇩

<div align="center">
  <img src="https://raw.githubusercontent.com/kevinadhiguna/strapi-graphql-documentation/master/assets/images/strapql.png" />
  <h3 align="center">Dokumentasi Strapi GraphQL API</h3>

  <p align="center">
    Kumpulan query dan mutation GraphQL pada aplikasi Strapi!
    <br />
    <a href="https://gitlab.com/kevinadhiguna/strapi-graphql-documentation#-table-of-contents"><strong>Eksplore sekarang »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/kevinadhiguna/strapi-graphql-documentation/-/issues">Laporkan Bug</a>
    ·
    <a href="https://gitlab.com/kevinadhiguna/strapi-graphql-documentation/-/issues">Request Fitur</a>
  </p>
</div>
<br>

<!-- DAFTAR ISI -->
## 📚 Daftar Isi

1. [Tentang Strapql](#-about-strapql)
2. [Query dan Mutation](#-queries-and-mutations)
    - [Registrasi / Daftar](#%EF%B8%8F-register)
    - [Login](#-login)
    - [Me Query](#-me-query)
      - [Cara melampirkan JWT pada headers](#-how-to-attach-jwt-in-headers-)
    - [Membuat user dalam Users](#-create-a-user-in-users-a-collection-type-that-comes-default-in-strapi-app)
      - [Cara mendapatkan JWT Superadmin](#-how-to-get-superadmins-jwt)
    - [Mendapatkan (Fetch) satu User](#-retrievefetch-a-single-user)
    - [Mendapatkan (Fetch) semua User](#-retrievefetch-all-users)
    - [Memperbarui (Update) satu User](#-update-a-user)
    - [Menghapus (Delete) satu User](#-deleteremove-a-user)
    - [Membuat (Create) data pada collection type](#-create-an-entry-in-a-collection-type)
    - [Mendapatkan (Fetch) data pada collection type](#-fetchretrieve-a-single-entry-in-collection-type)
    - [Mendapatkan (Fetch) semua data pada collection type](#-fetchretrieve-all-entries-in-collection-type)
    - [Memperbarui (Update) data pada collection type](#-update-an-entry-in-collection-type)
    - [Menghapus (Delete) data pada collection type](#-deleteremove-an-entry-in-collection-type)
    - [Mengunggah (Upload) satu gambar](#-%EF%B8%8F-upload-a-single-image)
    - [Mengunggah (Upload) beberapa gambar pada satu field](#-%EF%B8%8F-upload-multiple-images-in-a-single-field)
    - [Mengunggah (Upload) satu gambar pada beberapa field](#-%EF%B8%8F-upload-a-single-image-in-separate-fields)
      - [Bagaimana mutation `UploadSingleImageToSeveralFields` bekerja ?](#-how-does-uploadsingleimagetoseveralfields-mutation-work-)
    - [Mendapatkan semua file](#-get-all-files)
    - [Mendapatkan (Fetch) satu role](#-fetch-a-single-role)
    - [Mendapatkan (Fetch) semua role](#---fetch-all-roles)
3. [Membuat Kontribusi](#-contributing)
4. [Kontak](#-contact)
5. [Ucapan Terima Kasih](#-acknowledgements)

<!--
<ol>
  <li>
    <a href="#-about-strapql">About Strapql</a>
  </li>
  <li>
    <a href="#-queries-and-mutations">Queries and Mutations</a>
  </li>
  <li><a href="#-contributing">Contributing</a></li>
  <li><a href="#-contact">Contact</a></li>
  <li><a href="#-acknowledgements">Acknowledgements</a></li>
</ol>
-->

## 🌟 Tentang Strapql

Halo, selamat datang pada dokumentasi Strapi GraphQL API! Dokumentasi ini berisi beberapa query dan mutation yang dapat membantu anda jika anda menggunakan GraphQL API pada projek Strapi anda :)

## 🌈 Query dan Mutation

- Query digunakan untuk membaca atau mengambil nilai (`READ` /` RETRIEVE`).
- Mutasi mengubah data di penyimpanan data dan mengembalikan nilai. Ini dapat digunakan untuk menyisipkan, memperbarui, atau menghapus data (`CREATE`,` UPDATE`, dan `DELETE`).
<br> (Sumber : [TutorialsPoint](https://www.tutorialspoint.com/graphql/index.htm))

## ®️ Registrasi / Daftar
Seperti halnya aplikasi lain yang mewajibkan anda untuk membuat akun, anda harus mendaftar atau sign up terlebih dahulu untuk membuat user dalam collection type `users` yang tersedia secara default dalam Strapi. Berikut adalah cara untuk mendaftarkan akun :
```graphql
mutation Register($input: UsersPermissionsRegisterInput!) {
  register(input: $input) {
    jwt
    user {
      username
      email
    }
  }
}
```

Selanjutnya, masukkan `username`, `email`, dan `password` anda sebagai variabel :
```json
{
  "input": {
    "username": "USERNAME_ANDA",
    "email": "EMAIL_ANDA",
    "password": "PASSWORD_ANDA"
  }
}
```
Pada akhirnya, JWT muncul pada response.

## 🔒 Login
```graphql
mutation Login($input: UsersPermissionsLoginInput!) {
  login(input: $input) {
    jwt
    user {
      username
      email
      confirmed
      blocked
      role {
        id
        name
        description
        type
      }
    }
  }
}
```

Lalu masukkan `identifier` dan `password` anda sebagai variabel :
```json
{
  "input": {
    "identifier": "USERNAME_ANDA ATAU EMAIL_ANDA",
    "password": "PASSWORD_ANDA"
  }
}
```
Pada akhirnya, anda akan mendapat JWT pada response.

## 🙋 Me Query

Untuk mengidentifikasi suatu user, anda dapat menggunakan `me` query, seperti ini :
```graphql
query MeQuery {
  me {
    id
    username
    email
    confirmed
    blocked
    role {
      id
      name
      description
      type
    }
  }
}
```
<b>Catatan : `me` query mengharuskan JWT terlampir pada headers!</b>

### 📎 Cara melampirkan JWT pada headers :
`authorization : Bearer TOKEN_ANDA`

## 🆕 Membuat user dalam Users (collection type yang ada secara default dalam aplikasi Strapi)
> Loh, gimana? Membuat User? Bukannya tadi saya baru membuat user dengan mutation `Registration` di atas ?

Benar, ada beberapa hal penting dalam perbedaan kedua mutation tersebut :
|                                            |                `Create User` mutation                |                                   `Registration` mutation                                   |
|:------------------------------------------:|:----------------------------------------------------:|:-------------------------------------------------------------------------------------------:|
|       Perlu melampirkan JWT pada Headers?       | Ya, biasanya anda harus menjadi role `superadmin` dalam Strapi |                                              Tidak                                             |
| Apakah user yang akan dibuat akan terautentikasi di awal ? |                          Tidak                          | Ya, user yang dibuat dengan mutation `Registration` akan terautentikasi di awal |

```graphql
mutation CreateUser($input: createUserInput) {
  createUser(input: $input) {
    user {
      id
      createdAt
      updatedAt
      username
      email
      provider
      confirmed
      blocked
      role {
        id
        name
        description
        type
        permissions {
          type
          controller
          action
          enabled
          policy
          role {
            name
          }
        }
        users {
          username
        }
      }
    }
  }
}
```

Lampirkan variabel-variabel berikut :
```json
{
  "input": {
    "data": {
      "username": "USERNAME_ANDA",
      "email": "EMAIL_ANDA",
      "password": "PASSWORD_ANDA"
    }
  }
}
```
<b>Catatan : Tolong lampirkan JWT pada Headers, umumnya JWT milik superadmin.</b>

### 🔑 Cara mendapatkan JWT Superadmin

Klik `Documentation` pada menu di navbar kiri -> Copy token pada `Retrieve your jwt token`.

## 🧑 Mendapatkan (Fetch) satu User

Sebelumnya, kita sudah membuat user baru. Agar dapat mengambil data user tertentu dalam collection type `User`, anda dapat menggunakan query ini :
```graphql
query FetchSingleUser($id: ID!) {
  user(id: $id) {
    id
    createdAt
    updatedAt
    username
    email
    provider
    confirmed
    blocked
    role {
      name
    }
  }
}
```

Variabel yang dibutuhkan :
```json
{
  "id": "USER_ID_ANDA"
}
```

## 👥 Mendapatkan (Fetch) semua User

Jika anda ingin mengambil semua user dalam aplikasi Strapi anda, query berikut bisa jadi apa yang anda cari : 
```graphql
query FetchUsers {
  users {
    id
    createdAt
    updatedAt
    username
    email
    provider
    confirmed
    blocked
    role {
      name
    }
  }
}
```

Anda tidak perlu melampirkan variabel namun bisa jadi anda membutuhkan JWT terlampir pada headers (Ini tergantung pada roles & permissions di aplikasi Strapi anda).

## 🔄 Memperbarui (Update) satu User

Bayangkan anda ingin mengganti email users. Agar dapat melakukan hal seperti itu, anda sebaiknya menggunakan mutation yang memperbarui data user. Berikut adalah contoh untuk mengganti email user :
```graphql
mutation UpdateUser($input: updateUserInput) {
  updateUser(input: $input) {
    user {
      id
      createdAt
      updatedAt
      username
      email
      provider
      confirmed
      blocked
      role {
        name
      }
    }
  }
}
```

Kemudian sisipkan beberapa variabel yang anda ingin ganti (dalam hal ini, field `email`) :
```json
{
  "input": {
    "where": {
      "id": "ID_USER"
    },
    "data": {
      "email": "EMAIL_USER"
    } 
  }
}
```

Jika anda ingin mengganti field selain `email`, ganti `email` menjadi nama field yang anda ingin ganti.

## ❌ Menghapus (Delete) satu User

>Seorang user tidak lagi menggunakan aplikasi saya. Bagaimana saya menghapusnya dari aplikasi Strapi ?

Berikut adalah mutation yang dapat melakukan hal tersebut :
```graphql
mutation deleteUser($input: deleteUserInput) {
  deleteUser(input: $input) {
    user {
      id
      createdAt
      updatedAt
      username
      email
      provider
      confirmed
      blocked
      role {
        name
      }
    }
  }
}
```

Tempatkan ID user yang anda ingin hapus sebagai variabel :
```json
{
  "input": {
    "where": {
      "id": "ID_USER"
    }
  }
}
```

<b>Catatan : Berhati-hati agar mengatur role apa saja yang dapat melakukan operasi `delete` karena operasi tersebut sensitif.</b>

## 🆕 Membuat (Create) data pada collection type

Anggap anda telah membuat collection type bernama `idCardVerification`. Berikut adalah cara agar anda dapat menambah record di dalamnya :
```graphql
mutation createIdCardVerification($input: createIdCardVerificationInput) {
  createIdCardVerification(input: $input) {
    idCardVerification {
      id
      identifier
      birthPlace
    }
  }
}

```

Misalnya, `identifier` dan `birthPlace` adalah variabel yang ada pada collection type `idCardVerification`. Berikut adalah variabel yang anda sisipkan :
```json
{
  "input": {
    "data": {
      "identifier": "USERNAME_ANDA OR EMAIL_ANDA",
      "birthPlace": "London, United Kingdom"
    }
  }
}
```

<b>Catatan : `birthPlace: London, United Kingdom` adalah hanya contoh.</b>

## 📮 Mendapatkan (Fetch) data pada collection type

Agar dapat mengambil entry atau data pada collection type, query ini mungkin dapat membantu anda :
```graphql
query FetchSingleIdCardVerification($id: ID!) {
  idCardVerification(id: $id) {
    id
    identifier
    birthPlace
  }
}
```

Sisipkan ID record/entry yang ingin anda ambil :
```json
{
  "id": "ID_RECORD ATAU ID_ENTRY"
}
```

## 📒 Mendapatkan (Fetch) semua data pada collection type

Query ini dapat mengambil semua data pada collection type `idCardVerification` :
```graphql
query FetchIdCardVerifications {
  idCardVerifications {
    id
    identifier
    birthPlace
  }
}
```

## 🔄 Memperbarui (Update) data pada collection type

```graphql
mutation UpdateIdCardVerification($input: updateIdCardVerificationInput) {
  updateIdCardVerification(input: $input) {
    idCardVerification {
      id
      kind
      identifier
      birthPlace
    }
  }
}
```

Misal anda ingin mengganti nilai `birthPlace` ke California, United States. Sisipkan variabel berikut untuk melakukannya :
```json
{
  "input": {
    "where": {
      "id": "ID_RECORD ATAU ID_ENTRY"
    },
    "data": {
      "birthPlace": "California, United States"
    }
  }
}
```

Dalam hal ini, anda mengganti nilai pada field `birthPlace`. Response akan menampilkan nilai terbaru field `birthPlace`.

## ❌ Menghapus (Delete) data pada collection type

```graphql
mutation deleteIdCardVerification($input: deleteIdCardVerificationInput) {
  deleteIdCardVerification(input: $input) {
    idCardVerification {
      id
      identifier
      birthPlace
    }
  }
}
```

Variabel :
```json
{
  "input": {
    "where": {
      "id": "ID_RECORD ATAU ID_ENTRY"
    }
  }
}
```

## 📤 🖼️ Mengunggah (Upload) satu gambar

### ⚠️ Perhatian : Saat ini GraphQL Playground pada Strapi tidak support untuk mengunggah file/image. Anda dapat menggunakan GraphQL client lain untuk mencoba GraphQL upload mutation anda.
Salah satu GraphQL client yang saya gunakan adalah Altair. Anda dapat mengunduhnya di sini : https://altair.sirmuel.design/#download

Silahkan membuat record/entry baru pada collection type anda terlebih dahulu ! Jika tidak, file/gambar tidak akan terlampir/terunggah pada record/entry.
Catatan : `refId` adalah ID record atau entry yang anda buat pada collection type.
```graphql
mutation SingleImageUpload($refId: ID, $ref: String, $field: String, $file: Upload!) {
  upload(refId: $refId, ref: $ref, field: $field, file: $file) {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
}
```

Variabel :
```json
{
  "refId": "ID_RECORD ATAU ID_ENTRY PADA COLLECTION TYPE",
  "ref": "NAMA_COLLECTION_TYPE",
  "field": "NAMA_FIELD"
}
```

Berikut adalah contohnya :<br/>
<img src="https://raw.githubusercontent.com/kevinadhiguna/strapi-graphql-documentation/master/assets/gif/singleImageUpload.gif" />

## 📤 🖼️ Mengunggah (Upload) beberapa gambar pada satu field
```graphql
mutation MultipleImageUpload(
  $refId: ID
  $ref: String
  $field: String
  $files: [Upload]!
) {
  multipleUpload(refId: $refId, ref: $ref, field: $field, files: $files) {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
}
```

Variabel :
```json
{
  "refId": "ID_RECORD ATAU ID_ENTRY PADA COLLECTION_TYPE",
  "ref": "NAMA_COLLECTION_TYPE",
  "field": "NAMA_FIELD"
}
```

<b>Catatan : Dalam kasus ini, saya melampirkan gambar dengan name `files.0`, `files.1`, ... , `files.n` sebagai nama variabel hingga jumlah gambar yang anda ingin unggah (n).</b>

Berikut adalah contohnya :<br/>
<img src="https://raw.githubusercontent.com/kevinadhiguna/strapi-graphql-documentation/master/assets/gif/multipleImageUpload.gif" />

## 📤 🖼️ Mengunggah (Upload) satu gambar pada beberapa field
>Hmm... tetapi bagaimana saya mengunggah satu gambar ke beberapa fields dalam satu request?

Baiklah, bayangkan anda telah membuat collection type yang berisi beberapa field, termasuk `cardImage`, `facePhoto`, dan `personWithCardPhoto`. Jika tidak, ganti nama field-field tersebut dengan nama field milik anda. Ok, ini dia mutationnya :
```graphql
mutation UploadSingleImageToSeveralFields(
  $ref: String
  $refId: ID
  $cardImage: Upload!
  $facePhoto: Upload!
  $personWithCardPhoto: Upload!
) {
  cardImage: upload(
    ref: $ref
    refId: $refId
    field: "cardImage"
    file: $cardImage
  ) {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
  facePhoto: upload(
    ref: $ref
    refId: $refId
    field: "facePhoto"
    file: $facePhoto
  ) {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
  personWithCardPhoto: upload(
    ref: $ref
    refId: $refId
    field: "personWithCardPhoto"
    file: $personWithCardPhoto
  ) {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
}
```

Variabel :
```json
{
  "ref": "NAMA_COLLECTION_TYPE",
  "refId": "ID_RECORD ATAU ID_ENTRY PADA COLLECTION TYPE"
}
```

<b>Jangan lupa melampirkan file anda dengan nama variabel.</b><br>
Catatan: Dalam kasus ini, nama variabelnya adalah `cardImage`, `facePhoto`, dan `personWithCardPhoto`.

Berikut adalah contohnya :<br/>
<img src="https://raw.githubusercontent.com/kevinadhiguna/strapi-graphql-documentation/master/assets/gif/uploadSingleImageToSeveralFields.gif" />

### 🚀 Bagaiman mutation `UploadSingleImageToSeveralFields` bekerja ?

Pada mutation `UploadSingleImageToSeveralFields` di atas, anda tetap membutuhkan `ref`, `refId`, dan nama field. Namun demikian, anda mengirim sebuah request pada sebuah collection type dan anda juga sedang mencoba melampirkan gambar pada sebuah record atau entry dalam collection type. Jadi, anda dapat menyisipkan `ref` dan `refId` sebagai variabel. Nama field ? Anda sebaiknya menamakan itu secara statis karena anda ingin mengunggah sebuah gambar pada field yang berbeda. Semoga pendekatan ini dapat membantu :)

## 📂 Mendapatkan semua file
>Baik, I berhasil mengunggah gambar dan file pada aplikasi Strapi namun bagaimana saya tahu file apa saja yang saya unggah ?  

Untuk mendapatkan semua file yang diunggah ke database yang terhubung dengan aplikasi Strapi anda, berikut adalah querynya :
```graphql
query FetchFiles {
  files {
    id
    createdAt
    updatedAt
    name
    alternativeText
    caption
    width
    height
    formats
    hash
    ext
    mime
    size
    url
  }
}
```

Sayangnya, saat ini Strapi belum menyediakan query untuk mengambil satu file tertentu.

## 👨‍💻 Mendapatkan (Fetch) satu role

Berikut adalah query untuk mengambil satu role tertentu :
```graphql
query fetchSingleRole($id: ID!) {
  role(id: $id) {
    id
    name
    description
    type
    permissions {
      id
      type
      controller
      action
      enabled
      policy
      role {
        name
      }
    }
    users {
      id
      createdAt
      updatedAt
      username
      email
      provider
      confirmed
      blocked
      role {
        name
      }
    }
  }
}
```

Variabel :
```json
{
  "id": "ID_ROLE"
}
```

## 👨‍💻 👨‍💼 🧑‍🔧 Mendapatkan (Fetch) semua role

Di bawah ini adalah query untuk mengambil semua role :
```graphql
query FetchRoles {
  roles {
    id
    name
    description
    type
    permissions {
      id
      type
      controller
      action
      enabled
      policy
      role {
        name
      }
    }
    users {
      id
      createdAt
      updatedAt
      username
      email
      provider
      confirmed
      blocked
      role {
        name
      }
    }
  }
}
```

## 🖊 Membuat Kontribusi

Kontribusi adalah apa yang membuat komunitas open source menjadi tempat yang luar biasa untuk belajar, menginspirasi, dan berkreasi. Setiap kontribusi yang Anda berikan ** sangat dihargai **.

1. Fork projek ini
2. Membuat branch fitur (`git checkout -b feature/AmazingFeature`)
3. Commit perubahan yang anda buat (`git commit -m 'Add some Amazing Queries or Mutations'`)
4. Push pada branch tersebut (`git push origin feature/AmazingFeature`)
5. Membuat Pull Request

## 🌐 Kontak
Penulis : Kevin Adhiguna - [@kevinadhiguna](https://linkedin.com/in/kevinadhiguna) - hi.kevinadhiguna@gmail.com

Baca pada Blog : [https://about.lovia.life/docs/engineering/graphql/strapi-graphql-documentation/](https://about.lovia.life/docs/engineering/graphql/strapi-graphql-documentation/)

Kunjungi Github Gist : [https://gist.github.com/kevinadhiguna/623af7a87a629f672ca5bf7fe60a1d58](https://gist.github.com/kevinadhiguna/623af7a87a629f672ca5bf7fe60a1d58)

Link Projek : [https://github.com/kevinadhiguna/strapi-graphql-documentation](https://github.com/kevinadhiguna/strapi-graphql-documentation)

## 🎉 Ucapan Terima Kasih
* [Strapi](https://github.com/strapi/strapi)
* [Best-README-Template](https://github.com/othneildrew/Best-README-Template)
